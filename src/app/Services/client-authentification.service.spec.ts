import { TestBed } from '@angular/core/testing';

import { ClientAuthentificationService } from './client-authentification.service';

describe('ClientAuthentificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientAuthentificationService = TestBed.get(ClientAuthentificationService);
    expect(service).toBeTruthy();
  });
});
