import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ClientAuthentificationService} from '../../Services/client-authentification.service';

@Component({
  selector: 'embryo-SignIn',
  templateUrl: './CommonSignIn.component.html',
  styleUrls: ['./CommonSignIn.component.scss']
})
export class CommonSignInComponent implements OnInit {
  userForm = new FormGroup({


      pwd : new FormControl('',[Validators.required,Validators.minLength(3)]   ), //il va etre un input
      email: new FormControl('',[Validators.required]    ),
    }

  );
  constructor(private serviceClient:ClientAuthentificationService) { }

  ngOnInit() {
  }
  get pwdC(){
    return this.userForm.get('pwd') ;
  }
  get emailC(){
    return this.userForm.get('email') ;
  }
  seConnecter() {
    console.log("pwd = "+this.pwdC.value) ;
    console.log("email = "+this.emailC.value) ;
    this.serviceClient.authenticate(this.emailC.value,this.pwdC.value) ;

  }
}
